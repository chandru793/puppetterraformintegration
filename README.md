# Puppet Terraform Integration

In this we will see puppet terraform integration

# Prerequisites
- Terraform
- Puppet master running in VM or AWS
- Cloud providers such as AWS, Azure.

# Procedure
- Clone the repo or fork the repo and clone in your local
- Now execute the following commands in the terminal
    ```
    cd ./terraform
    terraform init
    terraform validate
    terraform plan
    terraform apply
    ```
- Paste the init.pp file under the manifest file of the puppet server
- Sign the puppet agent certificates from the puppet master instance


The Terraform script will create a fresh EC2 instance and automate the setup of Puppet Agent on it. Once the installation is finished, the Puppet Agent service will be initiated, and it will apply the catalog from the Puppet Server.