provider "aws" {
    region = "us-east-1"
}

resource "aws_instance" "puppetagent" {
    ami           = "ami-0440d3b780d96b29d"
    instance_type = "t2.micro"
    key_name      = "wl"

    tags = {
        Name = "puppetagent"
    }

    connection {
        type        = "ssh"
        user        = "ubuntu"
        private_key = file("wl.pem")
        host        = self.public_ip
        timeout     = "5m" # Adjust timeout as per your requirement
    }

    provisioner "remote-exec" {
        inline = [
            "sudo apt update",
            "sudo wget https://apt.puppetlabs.com/puppet8-release-bionic.deb",
            "sudo dpkg -i puppet8-release-bionic.deb",
            "sudo apt update",
            "sudo apt install puppet-agent -y",
            "sudo sh -c 'echo \"192.168.150.10 puppet\" >> /etc/hosts'",
            "sudo systemctl enable puppet",
            "sudo systemctl start puppet",
            "sudo /opt/puppetlabs/bin/puppet agent --test"
        ]
    }
}

output "public_ip" {
    value = aws_instance.puppetagent.public_ip
}